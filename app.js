require('dotenv').config();
const express = require('express');
const app = express();
const router = require('./routes/index.route');
const port = process.env.PORT;


app.use(express.json());

app.use(`${process.env.BASE_URL}`, router);

app.listen(port, () => {
    console.log(`Server telah berjalan di port ${port}`);
})