const express = require('express');
const router = express.Router();
const {authentication} = require("../misc/middleware");
const userRoutes = require('./user.route');
const userBiodataRoutes = require('./user_biodata.route');
const userHistoriesRoutes = require('./user_histories.route');
const swaggerJSON = require('../api-documentation/swagger.json');
const swaggerUI = require('swagger-ui-express');

router.get('/', (req, res) => {
    res.status(200).json({
        status: "Success",
        message: "Hello World!"
    });
});

router.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerJSON));

router.use(authentication);

router.use('/users', userRoutes);
router.use('/userBiodata', userBiodataRoutes);
router.use('/histories', userHistoriesRoutes);

module.exports = router;